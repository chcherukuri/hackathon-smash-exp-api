export function config(){
    return {
        port: process.env.PORT,
        appName: process.env.APP_NAME
    }
}