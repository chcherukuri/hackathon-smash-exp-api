import {LoadActiveConferencesRoute} from "../routes/loadActiveConferences.route";

require("dotenv-safe").load({ allowEmptyValues: false });
process.env.APP_NAME = "hackathon-smash-exp-api";

import { LogService } from "@pamperedchef/log-service";
import { CommonRoutingService } from "@pamperedchef/common-services/";
import { LoadConsultantInfoRoute } from "../routes/loadConsultantInfo.route";
import {LoadFeeInfoRoute} from "../routes/loadFeeInfo.route";
import {LoadToursInfoRoute} from "../routes/loadToursInfo.route";
import {LoadSpecialNeedsRoute} from "../routes/loadSpecialNeeds.route";
import {LoadInventoryPlanningInfoRoute} from "../routes/loadInventoryPlanningInfo.route";

// Get an instance of Express
const express = require("express");
const app = express();

// Setup logging
const log = new LogService();

// Set up cookie parser
app.use(require("cookie-parser")());

app.use((req, res, next) => {
    const origin = req.headers.origin;
    if (origin) {
        res.header("Access-Control-Allow-Origin", origin);
    }

    res.header("Access-Control-Allow-Credentials", "true");
    res.header("Access-Control-Allow-Headers", "X-Requested-With, Content-Type");
    next();
});

// The default page just displays some information
app.get("/", function (req, res) {
    res.send(process.env.APP_NAME + " is running here");
});

// Load routes
app.use("/" + process.env.APP_NAME + "/:version" + "/:locale",
    new CommonRoutingService("/loadConsultantInfo", new LoadConsultantInfoRoute()).registerGetRoute());
app.use("/" + process.env.APP_NAME + "/:version" + "/:locale",
    new CommonRoutingService("/loadActiveConferences", new LoadActiveConferencesRoute()).registerGetRoute());
app.use("/" + process.env.APP_NAME + "/:version" + "/:locale" + "/:conference",
    new CommonRoutingService("/loadFeeInfo", new LoadFeeInfoRoute()).registerGetRoute());
app.use("/" + process.env.APP_NAME + "/:version" + "/:locale" + "/:conference",
    new CommonRoutingService("/loadToursInfo", new LoadToursInfoRoute()).registerGetRoute());
app.use("/" + process.env.APP_NAME + "/:version" + "/:locale" + "/:conference",
    new CommonRoutingService("/loadSpecialNeeds", new LoadSpecialNeedsRoute()).registerGetRoute());
app.use("/" + process.env.APP_NAME + "/:version" + "/:locale" + "/:conference",
    new CommonRoutingService("/loadInventoryPlanningInfo", new LoadInventoryPlanningInfoRoute()).registerGetRoute());

let server = app.listen(process.env.PORT, () => {
    log.info("'" + process.env.APP_NAME + "' is listening on port " + server.address().port);
});

module.exports = server;
