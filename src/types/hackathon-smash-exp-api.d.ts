export interface ActiveConferencesResult {
    conferenceId: string;
    conferenceName: {
        en: string;
        de: string;
    }
}

export interface TourInfoResult {
    name: string;
    times: TourInfo[];
}

export interface TourInfo {
    fee: number;
    startTime: string;
    endTime: string;
    remainingCapacity: number;
}

export interface InventoryPlanningResult {
    pcgear: string[];
}
