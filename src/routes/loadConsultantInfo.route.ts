import {BaseRoute} from "@pamperedchef/common-services";
import {ErrorService} from "@pamperedchef/error-service";
import {HackathonSmashService} from "../services/hackathon-smash.service";
import {LogService} from "@pamperedchef/log-service";

export class LoadConsultantInfoRoute implements BaseRoute {
    public supportedVersions: string[] = ["v1"];

    private log : LogService =new LogService();
    private errorService: ErrorService = new ErrorService();
    private hackathonSmashService: HackathonSmashService = new HackathonSmashService();

    public getHackathonSmashService(): HackathonSmashService {
        return this.hackathonSmashService;
    }

    public getErrorService(): ErrorService {
        return this.errorService;
    }

    public async handleRoute(req, res) {
        try {
            const response = await this.hackathonSmashService.retrieveConsultantInfo(req);
            res.status(200);
            res.json(response);
            this.log.info("Request for Retrieve Consultant Info completed.");
        } catch (error) {
            this.errorService.handleRouteError(req, res, error, "LoadConsultantInfoRoute", true);
        }
    }
}
