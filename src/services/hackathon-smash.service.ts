import request = require("request-promise-native");
import { LogService } from "@pamperedchef/log-service";
import { CommonValidatorService } from "@pamperedchef/common-services/lib";
import * as fs from "fs";
import { promisify } from "util";
import {ActiveConferencesResult, InventoryPlanningResult, TourInfoResult} from "../types/hackathon-smash-exp-api";

export class HackathonSmashService {

    private logName = "Hackathon Smash Service: ";
    private log: LogService = new LogService();

    public retrieveConsultantInfo(req): Promise<any> {
        const globalID = (req.cookies.globalID);
        const url = process.env.HYBRIS_ENDPOINT + "/nh/consInfo?globalID=" + globalID;
        const options = {
            url: `${url}`,
            json: true,
            headers: {
                "Content-Type": "application/json",
                "Cookie": `JSESSIONID=${req.cookies.JSESSIONID}`
            }
        };

        //CommonValidatorService.validateNHCCAuthorizationCookies(req, true, true);
        this.log.info(this.logName + "Sending load consultant info request.");
        return request.get(options);
    }

    //Retrieve active conferences
    public retrieveActiveConferences(req): Promise<any> {
        let activeConferenceResult: ActiveConferencesResult[];
        const locale = req.cookies.NHCC_locale;
        let country="US";
        if(locale != null){
            country = locale.substring(3, 5);
        }
        const url = process.env.CONFERENCE_EVENT_PROCESS_API_ENDPOINT+ "/conference?country=" + country;
        const options = {
            url: `${url}`,
            json: true,
            headers: {
                "Content-Type": "application/json",
                "Cookie": `JSESSIONID=${req.cookies.JSESSIONID}`
            }
        };

        this.log.info(this.logName + "Sending retrieve active conferences request.");
        activeConferenceResult = request.get(options);
        let resp = new Promise((res, rej) => {
             res(activeConferenceResult);
        });
        return resp;
    }

    //Retrieve fee info
    public retrieveFeeInfo(req): Promise<any> {
        const url = process.env.CONFERENCE_EVENT_PROCESS_API_ENDPOINT+ "/fees";
        const payload = {
            consultantNumber: req.cookies.consultantNumber,
            conference: req.params.conference
        }
        const options = {
            url: `${url}`,
            body: payload,
            json: true,
            headers: {
                "Content-Type": "application/json",
                "Cookie": `JSESSIONID=${req.cookies.JSESSIONID}`
            }
        };

        this.log.info(this.logName + "Sending retrieve fee info request.");
        return request.get(options);
    }

    //Retrieve tours info
    public retrieveToursInfo(req): Promise<any> {
        let toursInfoResult : TourInfoResult[];
        const url = process.env.CONFERENCE_EVENT_PROCESS_API_ENDPOINT+ "/tours?conference:" + req.params.conference;
        const options = {
            url: `${url}`,
            json: true,
            headers: {
                "Content-Type": "application/json",
                "Cookie": `JSESSIONID=${req.cookies.JSESSIONID}`
            }
        };

        this.log.info(this.logName + "Sending retrieve fee info request.");
        toursInfoResult = request.get(options);
        let resp = new Promise((res, rej) => {
            res(toursInfoResult);
        });
        return resp;
    }

    //Retrieve Special needs
    public retrieveSpecialNeeds(req): Promise<any> {
        let specialNeeds: string[];
        const url = process.env.CONFERENCE_EVENT_PROCESS_API_ENDPOINT+ "/specialNeeds?conference:" + req.params.conference;
        const options = {
            url: `${url}`,
            json: true,
            headers: {
                "Content-Type": "application/json",
                "Cookie": `JSESSIONID=${req.cookies.JSESSIONID}`
            }
        };

        this.log.info(this.logName + "Sending retrieve fee info request.");
        specialNeeds = request.get(options);
        let resp = new Promise((res, rej) => {
            res(specialNeeds);
        });
        return resp;
    }

    //Retrieve Inventory Planning Info
    public retrieveInventoryPlanningInfo(req): Promise<any> {
        let inventoryPlanningResult: InventoryPlanningResult;
        const url = process.env.CONFERENCE_EVENT_PROCESS_API_ENDPOINT+ "/inventoryPlanning?conference:" + req.params.conference;
        const options = {
            url: `${url}`,
            json: true,
            headers: {
                "Content-Type": "application/json",
                "Cookie": `JSESSIONID=${req.cookies.JSESSIONID}`
            }
        };

        this.log.info(this.logName + "Sending retrieve fee info request.");
        inventoryPlanningResult = request.get(options);
        let resp = new Promise((res, rej) => {
            res(inventoryPlanningResult);
        });
        return resp;
    }
}
